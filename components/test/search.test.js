import { shallowMount } from '@vue/test-utils'
import axios from 'axios'
import flushPromises from 'flush-promises'
import Search from '@/components/Search.vue'

let wrapper

export const mockResponse = {
  symbol: 'AMZN',
  name: 'Amazon.com, Inc.',
  exchange: 'NASDAQ',
  trend: -0.016771
}

beforeEach(() => {
  wrapper = shallowMount(Search, {
    attachToDocument: true
  })
})

describe('Search component', () => {
  test('is a vue instance', () => {
    expect(wrapper.isVueInstance).toBeTruthy()
  })
  test('has a search input bar', () => {
    expect(wrapper.contains('.search-stock')).toBeTruthy()
  })
  test('has a search button', () => {
    expect(wrapper.contains('.bt-search-stock')).toBeTruthy()
  })
  test('search is working', async () => {
    const searchBar = wrapper.find('.search-stock')
    searchBar.element.value = 'amazon'
    searchBar.trigger('input')

    expect(wrapper.vm.searchQuery).toBe('amazon')

    wrapper.vm.$axios = axios
    wrapper.find('.bt-search-stock').trigger('click')
    await flushPromises()
    expect(wrapper.vm.stocks.length > 0).toBeTruthy()
    expect(wrapper.vm.stocks[0]).toMatchObject(mockResponse)
  })
})
